import math, random, binascii

dict = {
        'A' : 0, 
        'B' : 1,
        'C' : 2,
        'D' : 3,
        'E' : 4,
        'F' : 5,
        'G' : 6,
        'H' : 7,
        'J' : 8,
        'I' : 9,
        'K' : 10,
        'L' : 11,
        'M' : 12,
        'N' : 13,
        'O' : 14,
        'P' : 15,
        'Q' : 16,
        'R' : 17,
        'S' : 18,
        'T' : 19,
        'U' : 20,
        'V' : 21,
        'W' : 22,
        'X' : 23,
        'Y' : 24,
        'Z' : 25,
        ' ' : 26
    }

alphabetCode = 'ABCDEFGHJIKLMNOPQRSTUVWXYZ'
alphabetLength = 26

def menu():
    choice = 0
    while (choice != 3):
        print("Encryption and Cryptanalysis of RSA algorithm\n1. Encryption\n2. Cryptanalysis\n3. Exit")
        choice = int(input("Enter a choice: "))
        if choice == 1:
            print("----------Encryption----------\n")
            encryption()
            print()
        elif choice == 2:
            print("----------Cryptanalysis----------\n")
            cryptanalysis()
            print()
        elif choice == 3:
            print("Exit!")
        else:
            print("Invalid choice! Please try again!\n")

def totient(p, q):
	return (p-1)*(q-1)

def gcd(a, b):
    if b == 0:
        return a
    else:
        return gcd(b, a%b)

def gcdExtended(a, b):
    if a == 0:
        return (b, 0, 1)
    gcd, y, x = gcdExtended(b%a, a)
    return (gcd, x - (b // a) * y, y)

def multiplicative_inverse(a, m):
    g, x, y = gcdExtended(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

def encode_word(w):
    encoded_word = 0
    for letter in w:
        if letter in dict.keys():
            encoded_letter = dict[letter] * pow(26, (len(w) - w.index(letter) - 1))
            encoded_word += encoded_letter
    return encoded_word

def decode_word(w):
    final = []
    number2Encrypt = w
    testnum = 0
    for i in range(26):
        if w < pow(26,i):
            checkpoint = i
            break
    while testnum < checkpoint:
        curr = math.floor(number2Encrypt%26)
        final.append(alphabetCode[curr])
        number2Encrypt = number2Encrypt/26
        testnum = testnum + 1
    final.reverse()
    final = ''.join([str(x) for x in final])
    return final

def square_and_multiply(x, e, n):
    b = bin(e).lstrip('0b')
    r = 1
    for i in b:
        r = r**2
        if i == '1':
            r = r*x
        if n:
            r = r%n
    return r

def generate_public_key():
    c = []
    p = int(input("Enter a prime number: "))
    q = int(input("Enter another prime number: "))
    n = p*q
    phi = totient(p, q)
    for e in range(2, phi):
        if gcd(e, phi) == 1:
            c.append(e)
    e = c[random.randrange(0, len(c))]
    print("Your public key is: ({}, {})".format(e, n))
    return (e, n)

def encryption():
    cipher_text = []
    encoded_text = []
    plain_text = input("Enter plain text: ").upper()
    hex = binascii.hexlify(plain_text.encode())
    print("Plain text in hexadecimal: ", hex)
    words = plain_text.split()
    for word in words:
        encoded_text.append(encode_word(word))
    print("Encoded words are: ")
    print(' '.join([str(x) for x in encoded_text]))
    print("Generating public key...")
    e, n = generate_public_key()
    for i in encoded_text:
        cipher_text.append(square_and_multiply(i, e, n))
    print("Cipher text is: ")
    print(' '.join([str(x) for x in cipher_text]))

def cryptanalysis():
    plain_text = []
    encoded_words = []
    cipher_text = input("Enter cipher text: ")
    e, n = [int(x) for x in input("Enter public key: ").split()]
    p = int(input("Enter prime number p: "))
    q = int(input("Enter prime number q: "))
    t = (p-1)*(q-1)
    d = multiplicative_inverse(e, t)
    print("Your private key is: ({}, {})".format(d, n))
    data = cipher_text.split()
    for l in data:
        encoded_words.append(square_and_multiply(int(l), d, n))
    for word in encoded_words:
        plain_text.append(decode_word(word))
    print("Plain text is: ")
    print(' '.join([str(x) for x in plain_text]))

if __name__ == "__main__":
    menu()
